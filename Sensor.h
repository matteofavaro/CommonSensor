//
// Created by Matteo Favaro on 21/12/2016.
//

#ifndef SENSOR_H
#define SENSOR_H

#define MAXPARENTS 3


class Sensor {
  //const uint16_t address_;  // this is the RF24Network addres in octal form // not necessary
  // because on the network can be only one "sensorManager with that address"
  uint8_t sensor_id_; // the id is a number that could be 1 to 255
  // each group of sensor that are connected to a receiver
  // can have a numeration from 0 to 255
  // 1 - 255 -> 1 receiver the receiver has always the id 0
  // simultaneusly a receiver has a ID refered to its own super group
  const Sensor_type sensorType_;
  const Sensor_information_type sensorInformationType_;
  char name[31];//30 char for name I.E "motion stairs entrance 1" (24 char)
  Sensor *parent_;
  bool informationAvailable_;
  bool messageSent_;

 public:
  static Sensor *getSensorObj(MessageHelper *heartbeatMessage);
  Sensor(uint16_t const address,
         uint8_t const id,
         Sensor_type const type,
         Sensor_information_type const infotype);

  Sensor_type getSensorType();
  uint8_t getSensorID();
  void messageSent();
  void setMessageHasToSend();
  void setInformationAvaiable();
  void setInformationUnAvaiable();
  bool isInformationAvaiable();
  bool isMessageSent();
  virtual bool update() = 0;
  virtual bool hasToSend() = 0;
  virtual bool prepareDataMessageToSend(MessageHelper *message) = 0;

  void setSensorID(uint8_t id);
  uint16_t getSensorAddress() const;
  Sensor_information_type getSensorInformationType() const;

  Sensor *getParent();
  void setParent(Sensor *parent);

  void evaluateMessage(MessageHelper *message);

  // Communication methods
  //change all with the new style
  MessageHelper* getMessage();

 protected:
  virtual bool sensorChildrenPresentationReceived(MessageHelper *message) = 0;
  virtual bool sensorParentPresentationReceived(MessageHelper *message) = 0;
  virtual bool setSensorInfo(MessageHelper *message) = 0;
  virtual bool getSensorInfo(MessageHelper *message) = 0;
  virtual bool systemFuncion(MessageHelper *message) = 0;
  virtual bool incomingDataIsAStream(MessageHelper *message) = 0;
  //method for returning available command for this sensor;
};

#endif //SENSOR_H
