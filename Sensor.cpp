//
// Created by Matteo Favaro on 21/12/2016.
//

#include "Sensor.h"


///****** Sensor ******

Sensor::Sensor(uint16_t const address,
               uint8_t const sensor_id,
               Sensor_type const sensorType,
               Sensor_information_type const sensorInformationType)
    : address_(address),
      sensor_id_(sensor_id),
      sensorType_(sensorType),
      sensorInformationType_(sensorInformationType)
{
  informationAvailable_ = false;
  messageSent_ = false;
}

Sensor_type Sensor::getSensorType() {
  return sensorType_;
}
uint8_t Sensor::getSensorID() {
  return sensor_id_;
}
void Sensor::messageSent() {
  messageSent_ = true;
}
bool Sensor::isMessageSent() {
  return messageSent_;
}
void Sensor::setInformationAvaiable() {
  informationAvailable_ = true;
}
void Sensor::setInformationUnAvaiable() {
  informationAvailable_ = true;
}
bool Sensor::isInformationAvaiable() {
  return informationAvailable_;
}

void Sensor::setSensorID(uint8_t id) {
  sensor_id_ = id;
}

uint16_t Sensor::getSensorAddress() const {
  return address_;
}
Sensor_information_type Sensor::getSensorInformationType() const {
  return sensorInformationType_;
}

void Sensor::setParent(Sensor *parent) {
  parent_ = parent;
}

Sensor *Sensor::getParent() {
  return parent_;
}

void Sensor::setMessageHasToSend() {
  messageSent_ = false;
}

void Sensor::evaluateMessage(MessageHelper *message) {

  switch (message->getCommand()) {
    case C_PRESENTATION_CHILDREN:
      sensorChildrenPresentationReceived(message); //register the sensor if necessary
      break;
    case C_PRESENTATION_PARENT:
      sensorParentPresentationReceived(message);
      break;
    case C_SET:
      setSensorInfo(message); //based on sensor information type and ID set the data
      break;
    case C_REQ:
      getSensorInfo(message); //based on sensor information type and ID get the data
      break;
    case C_SYSTEM:
      systemFuncion(message); // set the sensor type, delay time, or other
      break;
    case C_STREAM:
      incomingDataIsAStream(message);// some kind or streaming data
      break;
    default:
      return;
  }
}




Sensor *Sensor::getSensorObj( MessageHelper *message) {

  switch(message->getSensorType()) {
    case S_LIGHT: return new RelaySensor(message->getSensorAddress(),
                                         message->getSensorID());
    case S_MOTION: return new MotionSensor(message->getSensorAddress(),
                                           message->getSensorID());
    case S_LIGHT_LEVEL: return new LightLevelSensor(message->getSensorAddress(),
                                                    message->getSensorID());
    default:
      return NULL;
  }
}